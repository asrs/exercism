def is_armstrong_number(number):
    lst = [int(i) for i in str(number)]
    return number == sum([x**len(lst) for x in lst])
