def classify(number):

  if number < 1:
    raise ValueError("positive value only")

  factor = [i for i in range(1, number) if number % i == 0]
  acc = sum(factor)

  if acc == number:
    return "perfect"
  elif acc < number:
    return "deficient"
  return "abundant"
