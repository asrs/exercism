def convert(number):
    mod = [(3, "Pling"), (5, "Plang"), (7, "Plong")]
    lst = []
    for i in mod:
      if (number % i[0]) == 0:
        lst.append(i[1])
    return "".join(lst) if lst else str(number)
