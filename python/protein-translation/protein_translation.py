catalog = {
    "AUG": "Methionine",
    "UUU": "Phenylalanine",
    "UUC": "Phenylalanine",
    "UUA": "Leucine",
    "UUG": "Leucine",
    "UCU": "Serine",
    "UCC": "Serine",
    "UCA": "Serine",
    "UCG": "Serine",
    "UAU": "Tyrosine",
    "UAC": "Tyrosine",
    "UGU": "Cysteine",
    "UGC": "Cysteine",
    "UGG": "Tryptophan",
}

def proteins(strand):
  lst = _chunk_by(strand.upper(), 3)
  ret = []
  for codon in lst:
    if codon in catalog:
      ret.append(catalog[codon])
    else:
      break
  return ret

def _chunk_by(strg, num):
  return [strg[i:i+3] for i in range(0, len(strg), num)]
