board = []

for i in range(0,65):
  if i > 1:
    board.append(board[i - 1] * 2)
  elif i == 0:
    board.append(0)
  else:
    board.append(1)

def square(number):
    if number < 1 or number > 64:
      raise ValueError("incorrect value given")
    return board[number]

def total():
    return sum(board)
