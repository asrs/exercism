translate = {"C": "G",
           "G": "C",
           "T": "A",
           "A": "U"}

def to_rna(dna_strand):
    return "".join([translate[nuc] for nuc in dna_strand])

