import random, string

class Robot:
    def __init__(self):
      self.libname = []
      self.name = self.generate_name()

    def generate_name(self):
      random.seed()
      p1 = ''.join(random.choices(string.ascii_uppercase, k=2))
      p2 = ''.join(random.choices(string.digits, k=3))
      name = p1 + p2
      if name not in self.libname:
        self.libname.append(name)
        return name
      return self.generate_name()

    def reset(self):
      self.name = self.generate_name()
