def score(x, y):
  radius = x ** 2 + y **2
  if radius > (10 **2):
    return 0
  elif radius > (5 ** 2):
    return 1
  elif radius > (1 ** 2):
    return 5
  return 10
