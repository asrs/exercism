class DiamondPrinter {

    val alphaPos = CharRange('A', 'Z')
    .zip( IntRange(1, 26) )
    .toMap()

    fun printToList(letter: Char): List<String> {
        require(letter in 'A'..'Z') { "ERROR" }

        val maxLen = alphaPos.get(letter)!!
        val topList = CharRange('A', letter).map({ buildString(it, maxLen) })
        val botList = topList.dropLast(1).reversed()

        return topList + botList
    }

    private fun buildString(letter: Char, len: Int): String {
        val refLen = len - alphaPos.get(letter)!!
        val spaceLen = (len - 1) - refLen
        val ref = " ".repeat(refLen)
        val space = " ".repeat(spaceLen)
        val ret = ref + letter + space

        return ret + ret.dropLast(1).reversed()
    }
}
