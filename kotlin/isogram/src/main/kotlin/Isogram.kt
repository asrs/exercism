object Isogram {
    fun isIsogram(input: String): Boolean {
        val graphemes = input.toLowerCase()
        .toCharArray()
        .filter({ it.isLetter() })
        .groupBy({ it })
        .map({ it.value.count() })
        .any({ it > 1 })

        return graphemes != true
    }
}
