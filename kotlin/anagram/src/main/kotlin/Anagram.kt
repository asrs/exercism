class Anagram(input : String) {
    private val source = input.toLowerCase()
    private val sortedSource = process(input)

    fun match(anagrams: Collection<String>): Set<String> {
        return anagrams.filter({ it.toLowerCase() != source })
        .filter({ process(it) == sortedSource })
        .toSet()
    }

    private fun process(input : String): List<Char> {
        return input .toLowerCase() .toList() .sorted()
    }
}
