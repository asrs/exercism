fun transcribeToRna(dna: String): String {
    return dna.map({ x -> process(x) })
    .joinToString("")
}

fun process(dna: Char): Char {
    return when (dna) {
        'G' -> 'C'
        'C' -> 'G'
        'T' -> 'A'
        'A' -> 'U'
        else -> dna
    }
}
