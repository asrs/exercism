object Pangram {
    private val alphaSet = ('a'..'z').toSet()

    fun isPangram(input: String): Boolean = input.toLowerCase().toSet().containsAll(alphaSet)
}
