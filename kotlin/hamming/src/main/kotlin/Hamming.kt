object Hamming {
    private val err1 = "left and right strands must be of equal length"

    fun compute(leftStrand: String, rightStrand: String): Int {
        require (leftStrand.length == rightStrand.length) { err1 }

        return leftStrand.zip( rightStrand )
        .filter({ it.first != it.second })
        .count()
    }
}
