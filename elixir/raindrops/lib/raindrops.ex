defmodule Raindrops do

  @factors [{"Pling", 3}, {"Plang", 5}, {"Plong", 7}]

  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """

  @spec convert(pos_integer) :: String.t()
  def convert(number) do
    data = process(number, "", @factors)

    case data do
      "" -> Integer.to_string(number)
      _ -> data
    end
  end

  @spec process(pos_integer, String.t, List.t) :: String.t()
  defp process(number, acc, [head | tail]) do
    if rem(number, elem(head, 1)) == 0 do
      process(number, acc <> elem(head, 0), tail)
    else
      process(number, acc, tail)
    end
  end

  defp process(_number, acc, []) do
    acc
  end
end
