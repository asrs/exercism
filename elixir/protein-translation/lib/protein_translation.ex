defmodule ProteinTranslation do

  @codons %{
    "UGU" => "Cysteine",
    "UGC" => "Cysteine",
    "UUA" => "Leucine",
    "UUG" => "Leucine",
    "AUG" => "Methionine",
    "UUU" => "Phenylalanine",
    "UUC" => "Phenylalanine",
    "UCU" => "Serine",
    "UCC" => "Serine",
    "UCA" => "Serine",
    "UCG" => "Serine",
    "UGG" => "Tryptophan",
    "UAU" => "Tyrosine",
    "UAC" => "Tyrosine",
    "UAA" => "STOP",
    "UAG" => "STOP",
    "UGA" => "STOP"
  }

  @doc """
  Given an RNA string, return a list of proteins specified by codons, in order.
  """
  @spec of_rna(String.t()) :: {atom, list(String.t())}
  def of_rna(rna) do
    case process(rna) do
      "invalid RNA" -> {:error, "invalid RNA"}
      n -> {:ok, n}
    end
  end

  @spec process(String.t()) :: list(String.t)
  defp process(rna) do
    rna
    |> String.codepoints
    |> Enum.chunk_every(3)
    |> Enum.reduce_while([], fn x, acc ->
      case Enum.join(x) |> of_codon do
        {:error, _} -> {:halt, "invalid RNA"}
        {:ok, "STOP"} -> {:halt, acc}
        {:ok, v} -> {:cont, acc ++ [v]}
      end
    end)
  end

  @doc """
  Given a codon, return the corresponding protein
  """
  @spec of_codon(String.t()) :: {atom, String.t()}
  def of_codon(codon) do
    case Map.fetch(@codons, codon) do
      {_, value} -> {:ok, value}
      _ -> {:error, "invalid codon"}
    end
  end

end
