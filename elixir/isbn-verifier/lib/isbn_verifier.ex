defmodule IsbnVerifier do
  @doc """
    Checks if a string is a valid ISBN-10 identifier

    ## Examples

      iex> ISBNVerifier.isbn?("3-598-21507-X")
      true

      iex> ISBNVerifier.isbn?("3-598-2K507-0")
      false

  """

  @spec isbn?(String.t()) :: boolean
  def isbn?(isbn) do
    case !Regex.match?(~R/[^\dX-]/, isbn) do
      true -> process(isbn)
      _  -> false
    end
  end

  defp process(isbn) do
    isbn
    |> String.replace("-", "")
    |> String.graphemes
    |> Enum.map(&(to_int(&1)))
    |> validate?
  end

  defp to_int("X"), do: 10
  defp to_int(str), do: String.to_integer(str)

  defp validate?(list) do
    list
    |> Enum.reduce({0, 10}, fn(x, acc) ->
      {val, index} = acc
      ret = x * elem(acc, 1)

      {val + ret, index - 1}
    end)
    |> elem(0)
    |> Integer.mod(11) == 0
  end

end
