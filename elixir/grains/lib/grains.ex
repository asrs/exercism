defmodule Grains do

  defguard is_chess(value) when value < 1 or value > 64

  @error "The requested square must be between 1 and 64 (inclusive)"

  @doc """
  Calculate two to the power of the input minus one.
  """
  @spec square(pos_integer) :: pos_integer
  def square(number) when is_chess(number), do: {:error, @error}

  def square(number) do
    value = floor(:math.pow(2, number - 1))
    {:ok, value}
  end

  @doc """
  Adds square of each number from 1 to 64.
  """
  @spec total :: pos_integer
  def total do
    value = floor(:math.pow(2, 64)) - 1
    {:ok, value}
  end

end
