defmodule Binary do
  @doc """
  Convert a string containing a binary number to an integer.

  On errors returns 0.
  """
  @spec to_decimal(String.t()) :: non_neg_integer
  def to_decimal(""), do: 0

  def to_decimal(string) do
    string
    |> String.to_charlist
    |> Enum.map(&(&1 - 48))
    |> valid?
    |> convert
  end

  defp valid?(list) do
    list
    |> Enum.map(&(&1 > 1 or &1 < 0))
    |> Enum.any?(&(&1 == true))
    |> case do
      true -> {:error, "invalid number"}
      false -> {:ok, list}
    end
  end

  defp convert({:error, _ }), do: 0

  defp convert({:ok, digits}) do
    digits
    |> to_base(2)
    |> from_base(10)
    |> Enum.reduce(0, fn x, acc -> acc * 10 + x end)
  end

  ## Get the number in the root base
  defp to_base(digits, base) do
    digits
    |> Enum.reduce(0, fn v, acc ->
      acc * base + v
    end)
  end

  ## This function will separate the number into list of the new base
  defp from_base(0, _), do: []

  defp from_base(digits, base) do
    ret = div(digits, base)

    from_base(ret, base) ++ [rem(digits, base)]
  end
end
