defmodule SpaceAge do
  @typedoc """
  Just a type for each valid planet
  """
  @type planet ::
          :mercury
          | :venus
          | :earth
          | :mars
          | :jupiter
          | :saturn
          | :uranus
          | :neptune

  @planet_list [
    :mercury,
    :venus,
    :earth,
    :mars,
    :jupiter,
    :saturn,
    :uranus,
    :neptune
  ]

  @multiplicator %{
    mercury: 0.2408467,
    venus: 0.61519726 ,
    earth: 1,
    mars: 1.8808158,
    jupiter: 11.862615,
    saturn: 29.447498,
    uranus: 84.016846 ,
    neptune: 164.79132
  }

  @earth_second 31_557_600

  @doc """
  Return the number of years a person that has lived for 'seconds' seconds is
  aged on 'planet'.
  """
  @spec age_on(planet, pos_integer) :: float
  def age_on(planet, seconds) when is_atom(planet)
  and is_integer(seconds)
  and planet in @planet_list do
    (seconds / @earth_second) / @multiplicator[planet]
  end
end
