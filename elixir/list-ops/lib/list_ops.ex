defmodule ListOps do
  # Please don't use any external modules (especially List or Enum) in your
  # implementation. The point of this exercise is to create these basic
  # functions yourself. You may use basic Kernel functions (like `Kernel.+/2`
  # for adding numbers), but please do not use Kernel functions for Lists like
  # `++`, `--`, `hd`, `tl`, `in`, and `length`.

  @spec count(list) :: non_neg_integer
  def count([]), do: 0
  def count([_ | td]), do: 1 + count(td)


  @spec reverse(list) :: list
  def reverse(l) do
    rev_loop(l, [])
  end

  defp rev_loop([], acc), do: acc
  defp rev_loop([hd | td], acc), do: rev_loop(td, [hd | acc])

  @spec map(list, (any -> any)) :: list
  def map(l, f) do
    for e <- l do
      f.(e)
    end
  end

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(l, f) do
    for e <- l, f.(e) do
      e
    end
  end

  @type acc :: any
  @spec reduce(list, acc, (any, acc -> acc)) :: acc
  def reduce([], acc, _), do: acc

  def reduce([hd | td], acc, f) do
    reduce(td, f.(hd, acc), f)
  end

  @spec append(list, list) :: list
  def append([], b), do: b
  def append([hd | td], b) do
    [hd | append(td, b)]
  end

  @spec concat([[any]]) :: [any]
  def concat([]), do: []

  def concat([hd | td]) do
    append(hd, concat(td))
  end
end
