defmodule Garden do
  @plants %{"V" => :violets, "R" => :radishes, "C" => :clover, "G" => :grass}

  @default ~w(alice bob charlie david eve fred ginny harriet ileana joseph kincaid larry)a

  @doc """
    Accepts a string representing the arrangement of cups on a windowsill and a
    list with names of students in the class. The student names list does not
    have to be in alphabetical order.

    It decodes that string into the various gardens for each student and returns
    that information in a map.
  """

  @spec info(String.t(), list) :: map
  def info(info_string, student_names \\ @default) do
    [rowleft, rowright] = info_string |> String.split("\n")

    left = rowleft |> String.graphemes()
    right = rowright |> String.graphemes()
    kids = student_names |> Enum.sort()

    process(left, right, kids, %{})
  end

  defp process(_, _, [], acc), do: acc

  defp process([] = tl, [] = tr, [kid | rest], acc) do
    ret = Map.put(acc, kid, {})

    process(tl, tr, rest, ret)
  end

  defp process([hl | [sl | tl]], [hr | [sr | tr]], [kid | rest], acc) do
    list = [hl, sl, hr, sr] |> Enum.map(&@plants[&1]) |> List.to_tuple()
    ret = Map.put(acc, kid, list)

    process(tl, tr, rest, ret)
  end
end
