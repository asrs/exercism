defmodule RobotSimulator do
  @moduledoc """
  This module is our RobotSimulator !
  """

  @enforce_keys [:position, :direction]
  defstruct position: {0, 0}, direction: :north

  @direction [:north, :east, :south, :west]
  @dir_clock %{:north => :east, :east => :south, :south => :west, :west => :north}
  @dir_counter %{:north => :west, :east => :north, :south => :east, :west => :south}

  #=======================================================================
  #-----------------------------------------------------------------------
  @spec create(direction :: atom, position :: {integer, integer}) :: any
  @doc """
  Create a Robot Simulator given an initial direction and position.

  Valid directions are: `:north`, `:east`, `:south`, `:west`
  """

  def create(direction \\ :north, position  \\ {0, 0})

  def create(direction, {x, y})
  when direction in @direction
  and is_integer(x)
  and is_integer(y) do
    %RobotSimulator{position: {x, y}, direction: direction}
  end

  def create(direction, _) when direction in @direction do
    {:error, "invalid position"}
  end

  def create(_, _position) do
    {:error, "invalid direction"}
  end

  #=======================================================================
  #-----------------------------------------------------------------------
  @spec simulate(robot :: any, instructions :: String.t()) :: any
  @doc """
  Simulate the robot's movement given a string of instructions.

  Valid instructions are: "R" (turn right), "L", (turn left), and "A" (advance)
  """

  def simulate(robot, instructions) do
    instructions
    |> String.graphemes
    |> Enum.reduce_while(robot, fn instruction, acc ->
      action(acc, instruction)
    end)
  end

  defp action(robot, instruction) do
    case instruction do
      "A" -> {:cont, advance(robot)}
      "R" -> {:cont, turn(robot, :clock)}
      "L" -> {:cont, turn(robot, :counter)}
      _ -> {:halt, {:error, "invalid instruction"}}
    end
  end

  defp advance(robot) do
    {x, y} = position(robot)

    case direction(robot) do
      :north -> create(:north, {x, y + 1})
      :south -> create(:south, {x, y - 1})
      :east -> create(:east, {x + 1, y})
      :west -> create(:west, {x - 1, y})
      _ -> robot
    end
  end

  defp turn(robot, :clock) do
    return = @dir_clock[direction(robot)]

    create(return, position(robot))
  end

  defp turn(robot, :counter) do
    return = @dir_counter[direction(robot)]

    create(return, position(robot))
  end

  #=======================================================================
  #-----------------------------------------------------------------------
  @spec direction(robot :: any) :: atom
  @doc """
  Return the robot's direction.
  """

  def direction(robot), do: Map.get(robot, :direction)

  #=======================================================================
  #-----------------------------------------------------------------------
  @spec position(robot :: any) :: {integer, integer}
  @doc """
  Return the robot's position.
  """

  def position(robot), do: Map.get(robot, :position)

end
