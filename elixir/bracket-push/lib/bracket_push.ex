defmodule BracketPush do

  @open ["(", "[", "{"]
  @close [")", "]", "}"]

  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """

  @spec check_brackets(String.t()) :: boolean
  @spec is_valid?(String.t, List.t) :: tuple
  @spec is_closing?(String.t, String.t, List.t) :: tuple

  def check_brackets(str) do
    str
    |> String.replace(~R/[^|\(\)\[\]\{\}]/, "")
    |> String.graphemes
    |> Enum.reduce_while([], &is_valid?(&1, &2))
    |> length() == 0
  end

  defp is_valid?(char, acc) do
    cond do
      char in @open -> {:cont, [char | acc]}
      char in @close -> is_closing?(List.first(acc), char, acc)
      true -> {:cont, acc}
    end
  end

  defp is_closing?(open, close, acc) do
    cond do
      open == "(" and close == ")" -> {:cont, tl(acc)}
      open == "[" and close == "]" -> {:cont, tl(acc)}
      open == "{" and close == "}" -> {:cont, tl(acc)}
      true -> {:halt, ["Mismatch brackets"]}
    end
  end

end
