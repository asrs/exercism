defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, b) do
    cond do
      a === b -> :equal
      contain?(b, a) -> :sublist
      contain?(a, b) -> :superlist
      true -> :unequal
    end
  end

  defp contain?(a, b) when length(a) < length(b), do: false
  defp contain?(a, b) do
    b === Enum.take(a, length(b)) or contain?(tl(a), b)
  end

end
