defmodule Hexadecimal do

  @values %{
    ?0 => 0,
    ?1 => 1,
    ?2 => 2,
    ?3 => 3,
    ?4 => 4,
    ?5 => 5,
    ?6 => 6,
    ?7 => 7,
    ?8 => 8,
    ?9 => 9,
    ?A => 10,
    ?B => 11,
    ?C => 12,
    ?D => 13,
    ?E => 14,
    ?F => 15
  }

  @doc """
    Accept a string representing a hexadecimal value and returns the
    corresponding decimal value.
    It returns the integer 0 if the hexadecimal is invalid.
    Otherwise returns an integer representing the decimal value.

    ## Examples

      iex> Hexadecimal.to_decimal("invalid")
      0

      iex> Hexadecimal.to_decimal("af")
      175

  """

  @spec to_decimal(binary) :: integer

  def to_decimal(""), do: 0

  def to_decimal(string) do
    string
    |> String.upcase
    |> String.to_charlist
    |> Enum.map(&(@values[&1]))
    |> valid?
    |> convert
  end

  defp valid?(list) do
    list
    |> Enum.map(&(&1 > 16 or &1 < 0))
    |> Enum.any?(&(&1 == true))
    |> case do
      true -> {:error, "invalid number"}
      false -> {:ok, list}
    end
  end

  defp convert({:error, _ }), do: 0

  defp convert({:ok, digits}) do
    digits
    |> to_base(16)
    |> from_base(10)
    |> Enum.reduce(0, fn x, acc -> acc * 10 + x end)
  end

  ## Get the number in the root base
  defp to_base(digits, base) do
    digits
    |> Enum.reduce(0, fn v, acc ->
      acc * base + v
    end)
  end

  ## This function will separate the number into list of the new base
  defp from_base(0, _), do: []

  defp from_base(digits, base) do
    ret = div(digits, base)

    from_base(ret, base) ++ [rem(digits, base)]
  end
end
