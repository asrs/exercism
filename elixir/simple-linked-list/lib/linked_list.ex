defmodule LinkedList do

  @opaque t :: tuple()
  @empty {}
  @err {:error, :empty_list}

  @doc """
  Construct a new LinkedList
  """
  @spec new() :: t
  def new(), do: @empty

  @doc """
  Push an item onto a LinkedList
  """
  @spec push(t, any()) :: t
  def push(list, elem), do: {elem, list}

  @doc """
  Calculate the length of a LinkedList
  """
  @spec length(t) :: non_neg_integer()
  def length(@empty), do: 0

  def length(list) do
    1 + __MODULE__.length(elem(list, 1))
  end

  @doc """
  Determine if a LinkedList is empty
  """
  @spec empty?(t) :: boolean()
  def empty?(list), do: list == @empty

  @doc """
  Get the value of a head of the LinkedList
  """
  @spec peek(t) :: {:ok, any()} | {:error, :empty_list}
  def peek({}), do: @err

  def peek(list), do: {:ok, elem(list, 0)}

  @doc """
  Get tail of a LinkedList
  """
  @spec tail(t) :: {:ok, t} | {:error, :empty_list}
  def tail({}), do: @err

  def tail(list), do: {:ok, elem(list, 1)}

  @doc """
  Remove the head from a LinkedList
  """
  @spec pop(t) :: {:ok, any(), t} | {:error, :empty_list}
  def pop({}), do: @err

  def pop(list) do
    {:ok, elem(list, 0), elem(list, 1)}
  end

  @doc """
  Construct a LinkedList from a stdlib List
  """
  @spec from_list(list()) :: t
  def from_list([]), do: {}

  def from_list(list) do 
    list
    |> Enum.reverse
    |> Enum.reduce(__MODULE__.new, &(push(&2, &1)))
  end

  @doc """
  Construct a stdlib List LinkedList from a LinkedList
  """
  @spec to_list(t) :: list()
  def to_list({}), do: []

  def to_list(list) do 
    [elem(list, 0)] ++ to_list(elem(list, 1))
  end

  @doc """
  Reverse a LinkedList
  """
  @spec reverse(t) :: t
  def reverse([]), do: []

  def reverse(list) do
    list
    |> __MODULE__.to_list
    |> Enum.reverse
    |> __MODULE__.from_list
  end
end
