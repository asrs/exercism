defmodule BeerSong do

  @verse_0 """
  No more bottles of beer on the wall, no more bottles of beer.
  Go to the store and buy some more, 99 bottles of beer on the wall.
  """

  @verse_1 """
  1 bottle of beer on the wall, 1 bottle of beer.
  Take it down and pass it around, no more bottles of beer on the wall.
  """

  @verse_2 """
  2 bottles of beer on the wall, 2 bottles of beer.
  Take one down and pass it around, 1 bottle of beer on the wall.
  """

  @spec verse(integer) :: String.t()
  @doc """
  Get a single verse of the beer song
  """

  def verse(0), do: @verse_0
  def verse(1), do: @verse_1
  def verse(2), do: @verse_2
  def verse(n), do: """
  #{n} bottles of beer on the wall, #{n} bottles of beer.
  Take one down and pass it around, #{n-1} bottles of beer on the wall.
  """


  @spec lyrics(Range.t()) :: String.t()
  @doc """
  Get the entire beer song for a given range of numbers of bottles.
  """

  def lyrics(range \\ 99..0) do
    range
    |> Enum.map_join("\n", &verse(&1))
  end

end
