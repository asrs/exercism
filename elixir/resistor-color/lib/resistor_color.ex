defmodule ResistorColor do
  @moduledoc false

  @resistor [
    "black",
    "brown",
    "red",
    "orange",
    "yellow",
    "green",
    "blue",
    "violet",
    "grey",
    "white",
  ]

  ### is this a joke ?

  @spec colors() :: list(String.t())
  def colors do
    @resistor
  end

  @spec code(String.t()) :: integer()
  def code(color) when is_binary(color) do
    Enum.find_index(colors(), &(&1 == color))
  end
end
