defmodule Luhn do
  @doc """
  Checks if the given number is valid via the luhn formula
  """
  @spec valid?(String.t()) :: boolean
  def valid?(number) do
    with true <- valid_format?(number) do
      number
      |> prepare
      |> process
      |> Kernel.==(0) # final checking
    end
  end

  defp valid_format?(num), do: !Regex.match?(~R/[^\s\d]/, num) # Check if there is only number and space

  defp prepare(number) do
    number
    |> String.replace(~R/[^\d]/, "") #removing any non-number
    |> String.replace(~R/^0/, "") #removing the first 0 to avoid reversing in luhn
    |> String.graphemes # go to list
    |> Enum.map(&String.to_integer/1) # go to integer
  end

  defp process([]), do: 1 # if the list is empty than mean it should be false

  defp process(list) do
    list
    |> Enum.map_every(2, &luhn/1) # Execute only on index divisible by 2
    |> Enum.sum # sum the number in the last
    |> rem(10) # is the number divisible by 10 ?
  end

  defp luhn(num) when num * 2 > 9, do: (num * 2) - 9
  defp luhn(num), do: num * 2
end
