defmodule OcrNumbers do

  @glyph %{
    [" _ ", "| |", "|_|", "   "] => "0",
    ["   ", "  |", "  |", "   "] => "1",
    [" _ ", " _|", "|_ ", "   "] => "2",
    [" _ ", " _|", " _|", "   "] => "3",
    ["   ", "|_|", "  |", "   "] => "4",
    [" _ ", "|_ ", " _|", "   "] => "5",
    [" _ ", "|_ ", "|_|", "   "] => "6",
    [" _ ", "  |", "  |", "   "] => "7",
    [ " _ ", "|_|", "|_|", "   "] => "8",
    [ " _ ", "|_|", " _|", "   "] => "9",
    [ " , ", " , ", " , ", " , "] => ",",
  }

  @doc """
  Given a 3 x 4 grid of pipes, underscores, and spaces, determine which number is represented, or
  whether it is garbled.
  """
  @spec convert([String.t()]) :: String.t()
  def convert(input) do
    with {:ok, _} <- row_valid?(input),
         {:ok, _} <- col_valid?(input)
    do
      input
      |> format
      |> process("")
      |> return
    end
  end

  defp row_valid?(input) do
    if rem(length(input), 4) == 0 do
      {:ok, input}
    else
      {:error, 'invalid line count'}
    end
  end

  defp col_valid?(input) do
    input
    |> Enum.all?(&rem(String.length(&1), 3) == 0)
    |> case do
      true -> {:ok, input}
      false -> {:error, 'invalid column count'}
    end
  end

  defp format(input) do
    input
    |> Enum.chunk_every(4)
    |> Enum.zip
    |> Enum.map(fn x -> x |> Tuple.to_list |> Enum.join(" , ") end)
  end


  defp process([
    <<h1 :: bytes-size(3), td1 :: binary>>,
    <<h2 :: bytes-size(3), td2 :: binary>>,
    <<h3 :: bytes-size(3), td3 :: binary>>,
    <<h4 :: bytes-size(3), td4 :: binary>>,
    ], acc) do
      ret = recognize([h1, h2, h3, h4]) || "?"

      process([td1, td2, td3, td4], acc <> ret)
  end

  defp process([
    <<_ :: bytes-size(0)>>,
    <<_ :: bytes-size(0)>>,
    <<_ :: bytes-size(0)>>,
    <<_ :: bytes-size(0)>>,
  ], acc) do
    acc
  end

  defp recognize(list), do: Map.get(@glyph, list)

  defp return(input) do
    {:ok, input}
  end

end
