defmodule Diamond do
  @doc """
  Given a letter, it prints a diamond starting with 'A',
  with the supplied letter at the widest point.
  """
  @spec build_shape(char) :: String.t()
  def build_shape(letter) do
    len = letter - ?@
    top = build_top(letter, len)
    bot = build_bot(top)

    Enum.concat(top, bot)
    |> Enum.join("\n")
    |> Kernel.<> "\n"
  end

  def build_top(letter, len) do
    Enum.map(len..1, fn x ->
      let = letter - x + 1
      ref = String.duplicate(" ", x - 1)
      size = ((len - x) * 2) + 1
      str = format(<<let>>, size)

      ref <> str <> ref
    end)
  end

  def build_bot(list) do
    list
    |> Enum.reverse
    |> Enum.drop(1)
  end

  defp format(str, size) when size > 1 do
    len = size - 2
    ref = String.duplicate(" ", len)

    str <> ref <> str
  end

  defp format(str, _), do: str

end
