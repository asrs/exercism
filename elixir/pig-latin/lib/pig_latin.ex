defmodule PigLatin do
  @doc """
  Given a `phrase`, translate it a word at a time to Pig Latin.

  Words beginning with consonants should have the consonant moved to the end of
  the word, followed by "ay".

  Words beginning with vowels (aeiou) should have "ay" added to the end of the
  word.

  Some groups of letters are treated like consonants, including "ch", "qu",
  "squ", "th", "thr", and "sch".

  Some groups are treated like vowels, including "yt" and "xr".
  """

  @vowels ~w(a e i o u)

  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    phrase
    |> String.split
    |> Enum.map(&pig_word(&1))
    |> Enum.join(" ")
  end

  def pig_word(word) do
    cond do
      String.starts_with?(word, @vowels) or is_yx(word) -> word <> "ay"
      String.contains?(word, "qu") -> qu_to_end(word) <> "ay"
      true -> consonant_to_end(word) <> "ay"
    end
  end

  defp is_yx(word) do
    String.first(word) in ["y", "x"] and String.at(word, 1) not in @vowels
  end

  defp qu_to_end(word) do
    [ head | tail ] = word
    |> String.split(~R/[{qu}?]/, include_captures: true, trim: true)
    |> Enum.reverse

    [ head | Enum.reverse(tail)]
    |> Enum.join
  end

  defp consonant_to_end(word) do
    [ head | tail ] = String.split(word, ~R/[a|e|i|o|u|y|x1?]/,include_captures: true, trim: true)

    Enum.join(tail ++ [head])
  end

end
