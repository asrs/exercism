defmodule Forth do
  alias __MODULE__, as: F

  @opaque evaluator :: Forth.t()

  @words %{
    "+": &Forth.add/1,
    "-": &Forth.sub/1,
    "*": &Forth.mul/1,
    "/": &Forth.div/1,
    dup: &Forth.dup/1,
    drop: &Forth.drop/1,
    swap: &Forth.swap/1,
    over: &Forth.over/1,
  }

  defstruct stack: [], words: @words

  @doc """
  Create a new evaluator.
  """
  @spec new() :: evaluator
  def new(), do: %F{}

  @doc """
  Evaluate an input string, launching execute/2
  """
  @spec eval(evaluator, String.t()) :: evaluator
  def eval(ev, s) do
    s
    |> format_string
    |> Enum.reduce(ev, &(execute(&1, &2)))
  end

  @doc """
  Execute/2, take a list of instruction, and an evaluator.
  It will update the evaluator state based on the list of instruction.
  """
  @spec execute(List.t(), evaluator) :: evaluator
  def execute([], ev), do: ev

  # We check if this element string start with ":", it mean it's word defining operation
  def execute(s = ":" <> _, %F{stack: stack, words: words}) do
    ret = s
    |> String.split
    |> define_word(words)

    %F{stack: stack, words: ret}
  end

  def execute(s, ev = %F{stack: stack, words: words}) do
    case Map.get(words, String.to_atom(s)) do
      nil -> exception(s, stack, words) # When the words is not found, we need more checking
      n when is_function(n) -> %F{stack: n.(stack), words: words} # When a words contain a function
      n when is_list(n) -> n |> Enum.reduce(ev, &(execute(&1, &2))) # When a words is map to operation
      n -> %F{stack: [n | stack], words: words}
    end
  end

  @spec exception(String.t(), List.t(), Map.t()) :: evaluator
  # if is not a number then is an undefined word
  defp exception(s, stack, words) do
    case Integer.parse(s) do
      :error -> raise(Forth.UnknownWord)
      _ -> %F{stack: [s | stack], words: words}
    end
  end

  #===========================================================
  #-----------------------------------------------------------
  # Formats

  @doc """
  Return the current stack as a string with the element on top of the stack
  being the rightmost element in the string.
  """
  @spec format_stack(evaluator) :: String.t()
  def format_stack(%F{stack: stack}) do
    stack
    |> Enum.reverse
    |> Enum.join(" ")
  end

  @doc """
  This function will take a string and will return a list of instruction strings
  """
  @spec format_string(String.t()) :: List.t()
  def format_string(s) do
    s
    |> String.downcase
    |> (&Regex.scan(~R/:.+;|[[:graph:]]+/u, &1)).()
    |> List.flatten
  end

  #===========================================================
  #-----------------------------------------------------------
  # Basic Arithmetic Operation

  @spec add(List.t()) :: List.t()
  def add([a | [b | td]]) do
    a = String.to_integer(a)
    b = String.to_integer(b)

    ["#{b + a}" | td]
  end
  def add(lst), do: lst

  @spec sub(List.t()) :: List.t()
  def sub([a | [ b | td]]) do 
    a = String.to_integer(a)
    b = String.to_integer(b)

    ["#{b - a}" | td]
  end
  def sub(lst), do: lst

  @spec mul(List.t()) :: List.t()
  def mul([a | [b | td]]) do
    a = String.to_integer(a)
    b = String.to_integer(b)

    ["#{b * a}" | td]
  end
  def mul(lst), do: lst

  @spec div(List.t()) :: List.t()
  def div([a | [b | _]]) when a == "0" or b == "0", do: raise(Forth.DivisionByZero)
  def div([a | [b | td]]) do
    a = String.to_integer(a)
    b = String.to_integer(b)

    ["#{Kernel.div(b ,a)}" | td]
  end
  def div(_lst), do: raise(Forth.DivisionByZero)

  #===========================================================
  #-----------------------------------------------------------
  # Forth Based Operation

  @spec dup(List.t()) :: List.t()
  def dup([]), do: raise(Forth.StackUnderflow)
  def dup([hd | td]), do: [hd | [hd | td]]

  @spec drop(List.t()) :: List.t()
  def drop([]), do: raise(Forth.StackUnderflow)
  def drop([_hd | td]), do: td

  @spec swap(List.t()) :: List.t()
  def swap([]), do: raise(Forth.StackUnderflow)
  def swap([a | [b | td]]), do: [b | [a | td]]
  def swap(_), do: raise(Forth.StackUnderflow)

  @spec over(List.t()) :: List.t()
  def over([]), do: raise(Forth.StackUnderflow)
  def over([hd | td]) do 
    a = hd |> String.to_integer

    if a > 1 do
      ["#{a - 1}" | [hd | td]]
    else
      raise(Forth.StackUnderflow)
    end
  end

  #===========================================================
  #-----------------------------------------------------------
  # Define Word Operations

  def define_word(s = [_ | [b | td]], words) do
    name = b |> String.to_atom
    lst = s |> Enum.slice(2..-2)
    chk = td |> Enum.join(" ")

    if Integer.parse(b) !== :error && Integer.parse(chk) != :error do
      raise(Forth.InvalidWord)
    else
      Map.update(words, name, lst, fn _ -> lst end)
    end
  end

  def define_word(_, words), do: words

  #===========================================================
  #-----------------------------------------------------------
  # Error Handling

  defmodule StackUnderflow do
    defexception []
    def message(_), do: "stack underflow"
  end

  defmodule InvalidWord do
    defexception word: nil
    def message(e), do: "invalid word: #{inspect(e.word)}"
  end

  defmodule UnknownWord do
    defexception word: nil
    def message(e), do: "unknown word: #{inspect(e.word)}"
  end

  defmodule DivisionByZero do
    defexception []
    def message(_), do: "division by zero"
  end
end
