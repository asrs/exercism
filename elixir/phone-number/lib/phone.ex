defmodule Phone do

  @error "0000000000"

  @doc """
  Remove formatting from a phone number.

  Returns "0000000000" if phone number is not valid
  (10 digits or "1" followed by 10 digits)

  ## Examples

  iex> Phone.number("212-555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 055-0100")
  "0000000000"

  iex> Phone.number("(212) 555-0100")
  "2125550100"

  iex> Phone.number("867.5309")
  "0000000000"
  """
  @spec number(String.t()) :: String.t()
  def number(raw) do
    raw
    |> format
    |> validate
  end

  defp format(raw) do
    raw
    |> String.replace(~R/^[\+1]|[\.\s\-\(\)]/, "")
  end

  def validate(n) when byte_size(n) == 10 do
    contain = Regex.match?(~R/[[:alpha:]]/, n)
    area = n |> String.at(0)
    exchange = n |> String.at(3)

    cond do
      contain == true -> @error
      String.to_integer(area) < 2 -> @error
      String.to_integer(exchange) < 2 -> @error
      true -> n
    end
  end

  def validate(n) when byte_size(n) == 11 do
    contain = Regex.match?(~r/[[:alpha:]]/, n)
    country = n |> String.at(0)
    area = n |> String.at(1)
    exchange = n |> String.at(4)

    cond do
      contain == true -> @error
      String.to_integer(country) != 1 -> @error
      String.to_integer(area) < 2 -> @error
      String.to_integer(exchange) < 2 -> @error
      true -> String.slice(n, 1..10)
    end
  end

  def validate(_), do: @error

  @doc """
  Extract the area code from a phone number

  Returns the first three digits from a phone number,
  ignoring long distance indicator

  ## Examples

  iex> Phone.area_code("212-555-0100")
  "212"

  iex> Phone.area_code("+1 (212) 555-0100")
  "212"

  iex> Phone.area_code("+1 (012) 555-0100")
  "000"

  iex> Phone.area_code("867.5309")
  "000"
  """
  @spec area_code(String.t()) :: String.t()
  def area_code(raw) do
    raw
    |> number
    |> String.slice(0..2)
  end

  @doc """
  Pretty print a phone number

  Wraps the area code in parentheses and separates
  exchange and subscriber number with a dash.

  ## Examples

  iex> Phone.pretty("212-555-0100")
  "(212) 555-0100"

  iex> Phone.pretty("212-155-0100")
  "(000) 000-0000"

  iex> Phone.pretty("+1 (303) 555-1212")
  "(303) 555-1212"

  iex> Phone.pretty("867.5309")
  "(000) 000-0000"
  """
  @spec pretty(String.t()) :: String.t()
  def pretty(raw) do
    data = raw |> number
    area = area_code(raw)
    exchange= data |> String.slice(3..5)
    rest= data |> String.slice(6..9)

    "(" <> area <> ") " <> exchange <> "-" <> rest
  end
end

# Phone.number("+1 (212) 555-0100")
