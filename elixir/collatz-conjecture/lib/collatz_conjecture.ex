defmodule CollatzConjecture do
  @doc """
  calc/1 takes an integer and returns the number of steps required to get the
  number to 1 when following the rules:
    - if number is odd, multiply with 3 and add 1
    - if number is even, divide by 2
  """
  @spec calc(input :: pos_integer()) :: non_neg_integer()

  def calc(input) when is_integer(input) and input > 0 do
    loop(input, 0)
  end

  defp loop(input, count) when input == 1, do: count

  defp loop(input, count) when input != 1 do
    loop(do_math(input), count + 1)
  end

  defp do_math(num) do
    if rem(num, 2) == 0 do
      div(num, 2)
    else
      (num * 3) + 1
    end
  end

end

