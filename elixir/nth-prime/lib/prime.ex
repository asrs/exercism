defmodule Prime do
  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer

  def nth(count) when count < 1, do: raise ArgumentError

  def nth(count) do
    2
    |> Stream.iterate(&(&1 + 1))
    |> Stream.filter(&(prime?(&1)))
    |> Stream.take(count)
    |> Enum.take(count)
    |> List.last
  end

  defp prime?(2), do: true

  defp prime?(n) do
    2..(n - 1)
    |> Enum.map(&(rem(n, &1) != 0))
    |> Enum.all?(&(&1 == true))
  end

end
