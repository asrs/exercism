defmodule Change do

  @err {:error, "cannot change"}

  @doc """
    Determine the least number of coins to be given to the user such
    that the sum of the coins' value would equal the correct amount of change.
    It returns {:error, "cannot change"} if it is not possible to compute the
    right amount of coins. Otherwise returns the tuple {:ok, list_of_coins}

    ## Examples

      iex> Change.generate([5, 10, 15], 3)
      {:error, "cannot change"}

      iex> Change.generate([1, 5, 10], 18)
      {:ok, [1, 1, 1, 5, 10]}

  """

  @spec generate(list, integer) :: {:ok, list} | {:error, String.t()}

  def generate(_coins, target) when target < 0, do: @err
  def generate(_coins, target) when target == 0, do: {:ok, []}

  def generate(coins, target) do
    {n1, c1} = coins |> Enum.sort |> Enum.reverse |> get_coins(target)
    {n2, c2} = coins |> get_best(target) |> get_coins(target)
    ret = if length(c1) < length(c2), do: {n1, c1}, else: {n2, c2}

    valid?(ret)
  end

  #============================================

  defp get_best(coins, target) do
    ret = coins
    |> Enum.filter(&(best?(target, &1)))
    |> Enum.reverse
    case Enum.empty?(ret) do
      true -> coins
      _ -> ret
    end
  end

  #============================================

  defp best?(n, divisor) do
    num = n - div(n, divisor) * divisor

    if num == 0 and divisor != 1, do: divisor
  end

  #============================================

  defp get_coins(coins, target) do
    Enum.reduce(coins, {target, []}, fn x, {n, acc} ->
      num = n - div(n, x) * x
      list = List.duplicate(x, div(n, x))

        {num, acc ++ list}
    end)
  end

  #============================================

  defp valid?({n, _}) when n > 0, do: @err
  defp valid?({_, []}), do: @err
  defp valid?({0, []}), do: @err

  defp valid?({0, list}) do
    {:ok, Enum.reverse(list)}
  end

end

# Change.generate([1, 5, 10], 18)
# |> IO.inspect

# coins = [1, 4, 15, 20, 50]
# Change.generate(coins, 23)
# |> IO.inspect

coins = [2, 2, 2, 5, 10]
Change.generate(coins, 21)
|> IO.inspect

# coins = [1, 5, 10, 21, 25]
# Change.generate(coins, 63)
# |> IO.inspect
