defmodule ETL do

  @doc """
  Transform an index into an inverted index.

  ## Examples

  iex> ETL.transform(%{"a" => ["ABILITY", "AARDVARK"], "b" => ["BALLAST", "BEAUTY"]})
  %{"ability" => "a", "aardvark" => "a", "ballast" => "b", "beauty" =>"b"}
  """
  @spec transform(map) :: map
  def transform(input) do
    input |> Enum.reduce(%{}, &(process(&1, &2)))
  end

  defp process({key, list}, acc) do
    list
    |> Enum.reduce(acc, fn x, acc ->
      Map.update(acc, String.downcase(x), key, &(&1 = key))
    end)
  end
end

