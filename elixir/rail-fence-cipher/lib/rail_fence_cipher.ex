defmodule RailFenceCipher do
  @doc """
  Encode a given plaintext to the corresponding rail fence ciphertext
  """
  @spec encode(String.t(), pos_integer) :: String.t()
  def encode(str, rails) when rails > byte_size(str), do: str

  def encode(str, rails) when rails == 1, do: str

  def encode("", _), do: ""

  def encode(str, rails) do
    list = str |> String.graphemes |> get_list(rails)

    Enum.map(1..rails, fn x ->
      Enum.filter(list, fn {c, i} -> i == x end)
    end)
    |> Enum.concat
    |> Enum.map(fn {c, _} -> c end)
    |> Enum.join
  end

  @doc """
  Decode a given rail fence ciphertext to the corresponding plaintext
  """
  @spec decode(String.t(), pos_integer) :: String.t()
  def decode(str, rails) when rails > byte_size(str), do: str

  def decode(str, rails) when rails == 1, do: str

  def decode("", _), do: ""

  def decode(str, rails) do
    len = String.length(str)

    get_list(0..len - 1, rails)
    |> Enum.sort(fn {_, a}, {_, b} -> a <= b end)
    |> Enum.zip(String.graphemes(str))
    |> Enum.sort
    |> Enum.map(fn {_, c} -> c end)
    |> Enum.join
  end

  #================================================

  defp get_list(lst, rails) do
    fence = get_fence(rails)
    len = length(fence)

    lst
    |> Enum.chunk_every(len)
    |> Enum.map(&Enum.zip(&1, fence))
    |> Enum.concat
  end

  defp get_fence(rails) do
    1..rails - 1 |> Enum.concat(rails..2)
  end
end
