type dna = [ `A | `C | `G | `T ]
type rna = [ `A | `C | `G | `U ]

let transpose = function
  | `A -> `U
  | `C -> `G
  | `G -> `C
  | `T -> `A

let to_rna lst = List.map transpose lst

