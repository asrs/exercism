open Base

let order s =
  s
  |> String.to_list
  |> List.sort ~compare:Char.compare
  |> String.of_char_list

let compare sa sb =
  let lsa = String.lowercase sa and lsb = String.lowercase sb in
  String.equal (order lsa) (order lsb) && not (String.equal lsa lsb)

let anagrams str candidates =
  candidates
  |> List.filter ~f:(fun x -> compare str x)

