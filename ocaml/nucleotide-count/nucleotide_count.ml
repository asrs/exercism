open Base

let empty = Map.empty (module Char)

let in_nuc = function
  | 'A' -> false
  | 'C' -> false
  | 'G' -> false
  | 'T' -> false
  | _ -> true

let check_nuc lst = List.filter lst ~f:(fun x -> in_nuc x)

let get_size lst c =
  List.filter lst ~f:(fun x -> Char.equal x c)
  |> List.length

let count_nucleotide s c =
  let lst = String.to_list s in
  let checked_lst = check_nuc lst in
  match checked_lst with
  | [] -> Ok (get_size lst, c)
  | hd :: _ -> Error hd

let count_nucleotides s =
  let nuc = ['A';'C';'G';'T'] in
  let clean_nuc = nuc |> List.filter ~f:(fun x -> String.contains s x)
  in
  Ok (clean_nuc
  |> List.map ~f:(fun x ->
      Map.set empty ~key:x ~data:(count_nucleotide s x)
    ))
